import {Component, OnInit} from '@angular/core';
import {NavController, normalizeURL} from 'ionic-angular';
import {Camera} from "@ionic-native/camera";
import {DomSanitizer} from "@angular/platform-browser";
import {DeviceMotion, DeviceMotionAccelerationData} from "@ionic-native/device-motion";
import {Subscription} from "rxjs";

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage implements OnInit {

  acceleration: DeviceMotionAccelerationData;
  imgUrl: any = "";

  accelerationSub: Subscription;

  constructor(public navCtrl: NavController,
              private camera: Camera,
              private deviceMotion: DeviceMotion,
              private domSanitizer: DomSanitizer) {

  }

  ngOnInit() {
    this.watchAcceleration();
  }

  takePicture() {
    this.camera.getPicture({
      quality: 50,
      correctOrientation: true,
      destinationType: this.camera.DestinationType.NATIVE_URI
    })
      .then((imageData) => {
          // this.imgUrl = this.domSanitizer.bypassSecurityTrustUrl(imageData);
          this.imgUrl = (<any>window).Ionic.WebView.convertFileSrc(imageData);

          this.watchAcceleration();
        },
        error => {
          console.log("NO PICTURE", error);
          this.watchAcceleration();
        });
  }

  private watchAcceleration() {
    if (this.accelerationSub == null) {
      this.accelerationSub = this.deviceMotion.watchAcceleration({
        frequency: 100
      })
        .subscribe((acceleration: DeviceMotionAccelerationData) => {
          this.acceleration = acceleration;
          this.checkAcceleration(acceleration);
        });
    }
  }

  private stopWatchAcceleration() {
    if (this.accelerationSub != null) {
      this.accelerationSub.unsubscribe();
      this.accelerationSub = null;
    }
  }

  private minY: number;
  private maxY: number;

  private checkAcceleration(acceleration: DeviceMotionAccelerationData) {
    if (this.maxY == null && acceleration.y < 8)
    {
      return;
    }

    if (this.minY == null || acceleration.y < this.minY) {
      this.minY = acceleration.y;
    }
    if (this.maxY == null || acceleration.y > this.maxY) {
      this.maxY = acceleration.y;
    }

    if ((this.maxY - this.minY) > 8 && acceleration.y > 8) {
      this.minY = null;
      this.maxY = null;
      this.stopWatchAcceleration();
      this.takePicture();
    }
  }
}
