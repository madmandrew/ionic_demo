import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

export interface Something
{
  label: string;
  value: number;
}

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  somethingSelected: number = 2;
  allTheThings: Something[] = [
    {
      label: "NES",
      value: 1
    },
    {
      label: "N64",
      value: 2
    },
    {
      label: "Playstation",
      value: 3
    },
    {
      label: "Xbox",
      value: 4
    }
  ];

  constructor(public navCtrl: NavController) {

  }

}
